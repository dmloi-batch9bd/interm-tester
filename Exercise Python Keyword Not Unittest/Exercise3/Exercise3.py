from selenium import webdriver
from selenium.webdriver.common.by import By, By
from selenium.webdriver.common.alert import Alert
from PythonKey import findElementByXPath
import Xpath
def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)
def Login(userID,password,btnLogin,txt_userID,txt_password):
    driver.find_element_by_xpath(userID).send_keys(txt_userID)
    driver.find_element_by_xpath(password).send_keys(txt_password)
    driver.find_element_by_xpath(btnLogin).click()
def ClickElement(xpath):
    driver.find_element_by_xpath(xpath).click()

def Advertisement(driver,xpathQuery):
    findElementByXPath(driver,xpathQuery)

def SelectNewCustomer(name, gender, DateBirth, Address, City, State, Pin, MobileNumber, email,Pass_Bank,
    txt_name, txt_DateBirth, txt_Address, txt_City, txt_State, txt_Pin, txt_MobileNumber, txt_email,txt_Pass_Bank):
    driver.find_element_by_xpath(name).send_keys(txt_name)
    driver.find_element_by_xpath(gender).click()
    driver.find_element_by_xpath(DateBirth).send_keys(txt_DateBirth)
    driver.find_element_by_xpath(Address).send_keys(txt_Address)
    driver.find_element_by_xpath(City).send_keys(txt_City)
    driver.find_element_by_xpath(State).send_keys(txt_State)
    driver.find_element_by_xpath(Pin).send_keys(txt_Pin)
    driver.find_element_by_xpath(MobileNumber).send_keys(txt_MobileNumber)
    driver.find_element_by_xpath(email).send_keys(txt_email)
    driver.find_element_by_xpath(Pass_Bank).send_keys(txt_Pass_Bank)

def CheckAddNew(name, gender, DateBirth, Address, City, State, Pin, MobileNumber, email,
    txt_name, txt_gender, txt_DateBirth, txt_Address, txt_City, txt_State, txt_Pin, txt_MobileNumber, txt_email):
    TableName = driver.find_element_by_xpath(name).text
    TableGender = driver.find_element_by_xpath(gender).text
    TableDateBirth = driver.find_element_by_xpath(DateBirth).text
    TableAddress = driver.find_element_by_xpath(Address).text
    TableCity = driver.find_element_by_xpath(City).text
    TableState = driver.find_element_by_xpath(State).text
    TablePin = driver.find_element_by_xpath(Pin).text
    TableMobileNumber = driver.find_element_by_xpath(MobileNumber).text
    Tableemail = driver.find_element_by_xpath(email).text
    isExist = False
    try:
        assert txt_name in TableName
        assert txt_gender in TableGender
        assert txt_DateBirth in TableDateBirth
        assert txt_Address in TableAddress
        assert txt_City in TableCity
        assert txt_State in TableState
        assert txt_Pin in TablePin
        assert txt_MobileNumber in TableMobileNumber
        assert txt_email in Tableemail
        isExist = True
        print("===========")
        print ("Verify add new customer's information correct with value input above: ", isExist)
        print("===========")

    except:
        isExist = False
        print("===========")
        print ("Verify add new customer's information correct with value input above: ", isExist)
        print("===========")
    return isExist

def ClickButtonLogout(btnlogout):
    btn = driver.find_element_by_xpath(btnlogout)
    driver.execute_script("arguments[0].click();", btn)

def HandleAlert():
    alert = Alert(driver)
    alert.accept()

def VerifyLoginPageDisplays(title):
    isExist = False
    try:
        assert title in driver.title
        isExist = True
        print("===========")
        print ("Verify login page displays: ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify login page displays: ", isExist)
        print("===========")
    return isExist
def Screenshots1(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise3/Check.png'
    driver.save_screenshot(the_path)

def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
Login(Xpath.userId,Xpath.password,Xpath.btnLogin,Xpath.txt_userId,Xpath.txt_password)
ClickElement(Xpath.New_Customer)
Advertisement(driver,Xpath.Customer_Name)
SelectNewCustomer(Xpath.Customer_Name,Xpath.Gender_female,Xpath.Date_of_Birth,Xpath.Address,Xpath.City,Xpath.State,Xpath.Pin,Xpath.Mobile_Number,Xpath.email,Xpath.Pass_Bank
, Xpath.txt_Customer_Name,Xpath.txt_Date_of_Birth,Xpath.txt_Address,Xpath.txt_City,Xpath.txt_State,Xpath.txt_Pin,Xpath.txt_Mobile_Number,Xpath.txt_email,Xpath.txt_Pass_Bank)
ClickElement(Xpath.Submit_New_Bank)
Screenshots1(driver)
CheckAddNew(Xpath.Table_Customer_Name,Xpath.Table_Gender,Xpath.Table_DateBirth,Xpath.Table_Address,Xpath.Table_City,Xpath.Table_State,Xpath.Table_Pin,Xpath.Table_Mobile,Xpath.Table_Email,
Xpath.txt_Customer_Name,Xpath.txt_Gender,Xpath.txt_Date_Table,Xpath.txt_Address,Xpath.txt_City,Xpath.txt_State,Xpath.txt_Pin,Xpath.txt_Mobile_Number,Xpath.txt_email)
ClickButtonLogout(Xpath.Log_out)
HandleAlert()
VerifyLoginPageDisplays(Xpath.title_home_page_login)
closeBrowserAndEndSession(driver)
    
