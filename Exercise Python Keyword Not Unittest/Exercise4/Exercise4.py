from select import select
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import Xpath
def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)
def ClickElement(MobileMenu):
    driver.find_element_by_xpath(MobileMenu).click()
def Screenshots(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise4/Check1.png'
    driver.save_screenshot(the_path)
def VerifySortByPosition(select):
    select_check = driver.find_element_by_xpath(select)
    select_option = Select(select_check)
    get_select = select_option.first_selected_option
    isExist = False
    try:
        assert "Position" in get_select.text
        isExist = True
        print("===========")
        print ("Verify sort by position: ",get_select.text,"-", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify sort by position: ", isExist)
        print("===========")
    return isExist
def VerifyPosition(ul_1st,ul_2st,ul_3st):
    ul_1st_place = driver.find_element_by_xpath(ul_1st)
    ul_2st_place = driver.find_element_by_xpath(ul_2st)
    ul_3st_place = driver.find_element_by_xpath(ul_3st)
    isExist = False
    try:
        assert "SONY XPERIA" in ul_1st_place.text
        assert "IPHONE" in ul_2st_place.text
        assert "SAMSUNG GALAXY" in ul_3st_place.text
        isExist = True
        print("===========")
        print ("Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY: ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY: ", isExist)
        print("===========")
    return isExist

def ClickSelectName(select):
    select_Name = Select(driver.find_element_by_xpath(select))
    select_Name.select_by_visible_text("Name")

def Screenshots1(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise4/Check2.png'
    driver.save_screenshot(the_path)

def VerifyName(ul_1st,ul_2st,ul_3st):
    ul_1st_place = driver.find_element_by_xpath(ul_1st)
    ul_2st_place = driver.find_element_by_xpath(ul_2st)
    ul_3st_place = driver.find_element_by_xpath(ul_3st)
    isExist = False
    try:
        assert "IPHONE" in ul_1st_place.text
        assert "SAMSUNG GALAXY" in ul_2st_place.text
        assert "SONY XPERIA"in ul_3st_place.text
        isExist = True
        print("===========")
        print ("Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA: ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA: ", isExist)
        print("===========")
    return isExist

def ClickSelectPrice(select):
    select_Name = Select(driver.find_element_by_xpath(select))
    select_Name.select_by_visible_text("Price")

def Screenshots2(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise4/Check3.png'
    driver.save_screenshot(the_path)

def VerifyPrice(ul_1st,ul_2st,ul_3st):
    ul_1st_place = driver.find_element_by_xpath(ul_1st)
    ul_2st_place = driver.find_element_by_xpath(ul_2st)
    ul_3st_place = driver.find_element_by_xpath(ul_3st)
    isExist = False
    try:
        assert "SONY XPERIA" in ul_1st_place.text
        assert "SAMSUNG GALAXY" in ul_2st_place.text
        assert "IPHONE" in ul_3st_place.text
        isExist = True
        print("===========")
        print ("Verify sort by price 1st SONY XPERIA 2st SAMSUNG GALAXY 3st IPHONE: ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify sort by price 1st SONY XPERIA 2st SAMSUNG GALAXY 3st IPHONE: ", isExist)
        print("===========")
    return isExist

def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
ClickElement(Xpath.Mobile_menu)
Screenshots(driver)
VerifySortByPosition(Xpath.select)
VerifyPosition(Xpath.ul_1st_place,Xpath.ul_2st_place,Xpath.ul_3st_place)
ClickSelectName(Xpath.select)
VerifyName(Xpath.ul_1st_place,Xpath.ul_2st_place,Xpath.ul_3st_place)
Screenshots1(driver)
ClickSelectPrice(Xpath.select)
VerifyPrice(Xpath.ul_1st_place,Xpath.ul_2st_place,Xpath.ul_3st_place)
Screenshots2(driver)
closeBrowserAndEndSession(driver)
