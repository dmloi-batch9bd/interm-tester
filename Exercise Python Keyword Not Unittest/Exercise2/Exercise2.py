from selenium import webdriver
import Xpath
def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)

def Verify(xpath,a):
    isExist = False
    try:
        name = driver.find_element_by_xpath(xpath).text
        assert a in name
        location = driver.find_element_by_xpath(xpath).location
        isExist = True
        print("===========")
        print ("Verify position of ",name,location, isExist)
        print("===========")
    except:
        isExist = False
        name = driver.find_element_by_xpath(xpath).text
        print("===========")
        print ("Verify position of ",name, isExist)
        print("===========")
    return isExist

def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
# Verify position of Action
# Verify column Action stay on the left of column Bank Name
Verify(Xpath.Action,Xpath.a)
# Verify position of Bank Name
# Verify column Bank Name stay on the right of column Action
Verify(Xpath.Bank_Name,Xpath.b)
# Verify cell Bank 3 stay under column Bank Name
Verify(Xpath.Bank3,"Bank 3")
# Verify cell Edit stay on the left of cell bank 3
Verify(Xpath.Edit_Bank3,"Edit")
# Row 1 should be Bank 1 under Bank Name column
Verify(Xpath.row_1,"Edit Bank 1")
# Row 2 should be Bank 1 under Bank Name column
Verify(Xpath.row_2,"Edit Bank 2")
# Row 3 should be Bank 3 under Bank Name column
Verify(Xpath.row_3,"Edit Bank 3")
closeBrowserAndEndSession(driver)