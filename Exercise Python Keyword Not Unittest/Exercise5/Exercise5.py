from selenium import webdriver
import Xpath
def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)

def ClickElement(xpath):
    driver.find_element_by_xpath(xpath).click()

def UpdateQuantity(quantity,number):
    driver.find_element_by_xpath(quantity).clear()
    driver.find_element_by_xpath(quantity).send_keys(number)

def ApplyCode(code,txtCode,btnApply):
    driver.find_element_by_xpath(code).send_keys(txtCode)
    driver.find_element_by_xpath(btnApply).click()

def VerifyDiscountValueCode(Subtotal,Discount):
    isExist = False
    try:
        GetSubtotal = driver.find_element_by_xpath(Subtotal).text
        GetDiscount = driver.find_element_by_xpath(Discount).text
        number_Subtotal = int(GetSubtotal[1:-3])
        number_Discount = int(GetDiscount[2:-3])
        discount_value =  number_Discount/number_Subtotal  * 100
        isExist = True
        print ("Discount value base on coupon code:",discount_value ,"%  ",isExist)
    except:
        isExist = False
        print("Verify discount value base on coupon code",isExist)
    return isExist
    
def Screenshots(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise5/Check.png'
    driver.save_screenshot(the_path)

def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
ClickElement(Xpath.Mobile_menu)
ClickElement(Xpath.Btn_Add_to_iphone)
ClickElement(Xpath.btn_continue)
ClickElement(Xpath.Btn_Add_to_samsung)
UpdateQuantity(Xpath.number_samsung_cart,"2")
ClickElement(Xpath.btn_update)
ApplyCode(Xpath.code_cart,Xpath.txt_code,Xpath.btn_apply_code)
VerifyDiscountValueCode(Xpath.Subtotal,Xpath.Discount)
Screenshots(driver)
closeBrowserAndEndSession(driver)


