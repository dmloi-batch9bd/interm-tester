from selenium import webdriver
from selenium.webdriver.common.by import By, By
from PythonKey import findElementByXPath
import Xpath
def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)

def Verify_The_Message_Displayed(driver, username,password,btnLogin):
    driver.find_element_by_xpath(username).click()
    driver.find_element_by_xpath(password).click()
    driver.find_element_by_xpath(btnLogin).click()

def Check_Verify_The_Message_Displayed(driver, Mess_id,Mess_pass):
    test = driver.find_element(By.XPATH,Mess_id).is_displayed()
    test2 = driver.find_element(By.XPATH,Mess_pass).is_displayed()
    isExist = False
    try:
        test == True
        test2 == True
        isExist = True
        print("===========")
        print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty:  ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty:  ", isExist)
        print("===========")
    return isExist

def Screenshots(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise1/VerifyTheMessageHasDisappeared.png'
    driver.save_screenshot(the_path)

def Verify_The_Message_Not_Displayed(userID,password,txt_userID,txt_pass):
    driver.find_element_by_xpath(userID).send_keys(txt_userID)
    driver.find_element_by_xpath(password).send_keys(txt_pass)
    driver.find_element_by_xpath(userID).click()

def Check_Verify_The_Message_Not_Displayed( Mess_id,Mess_pass):
    test = driver.find_element(By.XPATH,Mess_id).is_displayed()
    test2 = driver.find_element(By.XPATH,Mess_pass).is_displayed()
    isExist = False
    try:
        test == False
        test2 == False
        isExist = True
        print("===========")
        print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared:  ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared:  ", isExist)
        print("===========")
    return isExist

def Screenshots1(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise1/Check.png'
    driver.save_screenshot(the_path)

def Click_Button_Rest(btn_reset):
    driver.find_element_by_xpath(btn_reset).click()

def Check_Clear(userID,password):
    text_user = driver.find_element_by_xpath(userID).text
    text_pass = driver.find_element_by_xpath(password).text
    isExist = False
    try:
        not text_user
        not text_pass
        isExist = True
        print("===========")
        print ("Verify userID and password is cleared:  ", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify userID and password is cleared:  ", isExist)
        print("===========")
    return isExist

def Screenshots2(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise1/Check_btnReset.png'
    driver.save_screenshot(the_path)

def Login(userID,password,txt_userID,txt_pass,btnlogin):
    driver.find_element_by_xpath(userID).send_keys(txt_userID)
    driver.find_element_by_xpath(password).send_keys(txt_pass)
    driver.find_element_by_xpath(btnlogin).click()

def Check_Login(user,xpathQuery,title):
    isExist = False
    try:
        findElementByXPath(driver,xpathQuery)
        findElementByXPath
        user = driver.find_element_by_xpath(user).text
        assert title in driver.title
        isExist = True
        print("===========")
        print ("Verify login to home page successful:  ", isExist)
        print(user)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify login to home page successful:  ", isExist)
        print("===========")
    return isExist

def Screenshots3(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise1/Check_login.png'
    driver.save_screenshot(the_path)
def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
Verify_The_Message_Displayed(driver,Xpath.userId,Xpath.password,Xpath.btnLogin)
Check_Verify_The_Message_Displayed(driver,Xpath.Blank_id,Xpath.Blank_pass)
Screenshots(driver)
Verify_The_Message_Not_Displayed(Xpath.userId,Xpath.password,Xpath.txt_userId,Xpath.txt_password)
Check_Verify_The_Message_Not_Displayed(Xpath.Blank_id,Xpath.Blank_pass)
Screenshots1(driver)
Click_Button_Rest(Xpath.btnreset)
Check_Clear(Xpath.userId,Xpath.password)
Screenshots2(driver)
Login(Xpath.userId,Xpath.password,Xpath.txt_userId,Xpath.txt_password,Xpath.btnLogin)
Check_Login(Xpath.userId_login,Xpath.userId_login,Xpath.title)
Screenshots3(driver)
closeBrowserAndEndSession(driver)

