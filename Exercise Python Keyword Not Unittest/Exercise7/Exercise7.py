from selenium import webdriver
from selenium.webdriver.support.ui import Select
from PythonKey import findElementByXPath
import re
import Xpath

def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)

def ClickElement(xpath):
    driver.find_element_by_xpath(xpath).click()

def SwitchToWindows(driver):
    window_after = driver.window_handles[1]
    driver.switch_to.window(window_after)

def GetTextYourCard(CardNumber,CardCW,CardExp):
    NumberCard = driver.find_element_by_xpath(CardNumber).text
    CWCard = driver.find_element_by_xpath(CardCW).text
    ExpCard = driver.find_element_by_xpath(CardExp).text
    global number_card
    number_card = re.sub(r'\D', "", NumberCard)
    global cw_card
    cw_card = re.sub(r'\D', "", CWCard)
    Exp = re.sub(r'\D', "", ExpCard)
    global exp_year
    exp_year = Exp[2:]
    global exp_month
    exp_month = Exp[:-4]

def CheckyourCard(enterCard):
    driver.find_element_by_xpath(enterCard).send_keys(number_card)

def Screenshots(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise7/Check1.png'
    driver.save_screenshot(the_path)

def BackWindows(driver):
    driver.close()
    window_after = driver.window_handles[0]
    driver.switch_to.window(window_after)

def SelectQuantity(Quantity,numberSelect):
    select = Select(driver.find_element_by_xpath(Quantity))
    select.select_by_visible_text(numberSelect)

def ScreenshotsSelect(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise7/check_select.png'
    driver.save_screenshot(the_path)

def InputCard(buyCardNumber,ExpirationMonth,ExpirationYear,CWCode):
    driver.find_element_by_xpath(buyCardNumber).send_keys(number_card)
    select_exp_month = Select(driver.find_element_by_xpath(ExpirationMonth))
    select_exp_month.select_by_visible_text(exp_month)
    select_exp_year = Select(driver.find_element_by_xpath(ExpirationYear))
    select_exp_year.select_by_visible_text(exp_year)
    driver.find_element_by_xpath(CWCode).send_keys(cw_card)

def Screenshots1(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise7/Check2.png'
    driver.save_screenshot(the_path)

def Check(xpath):
    isExist = False
    try:
        driver.find_element_by_xpath(xpath).click()
        isExist = True
        print("===========")
        print ("Verify Payment Gateway Project", isExist)
        print("===========")
    except:
        isExist = False
        print("===========")
        print ("Verify Payment Gateway Project", isExist)
        print("===========")
    return isExist

def Screenshots2(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise7/Check3.png'
    driver.save_screenshot(the_path)

def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
ClickElement(Xpath.Payment_Gateway_Project)
findElementByXPath(driver,Xpath.Generate_Card_Number)
ClickElement(Xpath.Generate_Card_Number)
SwitchToWindows(driver)
GetTextYourCard(Xpath.Card_number,Xpath.Card_CW,Xpath.Card_Exp)
ClickElement(Xpath.Check_Credit_Card)
findElementByXPath(driver,Xpath.Enter_Credit_Card)
CheckyourCard(Xpath.Enter_Credit_Card)
ClickElement(Xpath.btn__Credit_Card)
Screenshots(driver)
BackWindows(driver)
SelectQuantity(Xpath.Quantity,"3")
ScreenshotsSelect(driver)
ClickElement(Xpath.btn_Buy_Now)
InputCard(Xpath.buy_card_number,Xpath.Expiration_Month,Xpath.Expiration_Year,Xpath.CW_code)
Screenshots1(driver)
Check(Xpath.btn_pay)
Screenshots2(driver)
closeBrowserAndEndSession(driver)




    
