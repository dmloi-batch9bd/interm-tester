from selenium import webdriver
from selenium.webdriver.support.ui import Select
from PythonKey import findElementByXPath
import Xpath

def OpenBrowser(url):
    global driver
    driver = webdriver.Chrome()
    driver.get(url)

def ClickElement(AgileProject):
    driver.find_element_by_xpath(AgileProject).click()

def Login(userID, password,txt_user,txt_pass):
    findElementByXPath(driver,userID)
    driver.find_element_by_xpath(userID).send_keys(txt_user)
    driver.find_element_by_xpath(password).send_keys(txt_pass)

def SelectClick(selectAgile,AccountNo):
    findElementByXPath(driver,selectAgile)
    select = Select(driver.find_element_by_xpath(selectAgile))
    select.select_by_visible_text(AccountNo)

def Screenshots(driver):
    the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword Not Unittest/Exercise6/Check.png'
    driver.save_screenshot(the_path)

def closeBrowserAndEndSession(driver):
    driver.close()
    driver.quit()

OpenBrowser(Xpath.url)
ClickElement(Xpath.Agile_Project)
Login(Xpath.userId,Xpath.password,Xpath.txt_user_Agile,Xpath.txt_pass_Agile)
ClickElement(Xpath.btnLogin)
ClickElement(Xpath.Mini_Statement)
SelectClick(Xpath.Mini_Account,Xpath.Account_no)
ClickElement(Xpath.Submit_Agile)
Screenshots(driver)
closeBrowserAndEndSession(driver)