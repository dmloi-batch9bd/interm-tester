from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By, By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
    
def checkElementExistByXPath(driver, xpathQuery):
    isExist = False
    try:
        findElementByXPath(driver, xpathQuery)
        isExist = True
    except:
        isExist = False
    return isExist

def navigateToPage(driver, page):
    driver.get(page)   

def findElementByName(driver, name):    
    element = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, name)))
    return element

def findElementByXPath(driver, xPath):    
    element = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, xPath)))
    return element

