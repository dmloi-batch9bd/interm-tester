import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support import expected_conditions as EC
import Xpath
import Values

class VerifyAddNewCustomer(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(Xpath.url)
    def test_case(self):
        #Login
        driver = self.driver
        userId = driver.find_element(By.XPATH, Xpath.userId)
        password = driver.find_element(By.XPATH, Xpath.password)
        btnLogin = driver.find_element(By.XPATH, Xpath.btnLogin)
        userId.send_keys(Values.userId)
        password.send_keys(Values.password)
        btnLogin.click()
        new_Customer = driver.find_element(By.XPATH, Xpath.New_Customer)
        new_Customer.click()
        #Imput textboox ADD New Customer
        txt_Name = driver.find_element(By.XPATH, Xpath.Customer_Name)
        txt_Gender_female = driver.find_element(By.XPATH, Xpath.Gender_female)
        txt_Data_of_Birth = driver.find_element(By.XPATH,Xpath.Date_of_Birth)
        txt_Address = driver.find_element(By.XPATH, Xpath.Address)
        txt_City = driver.find_element(By.XPATH, Xpath.City)
        txt_State = driver.find_element(By.XPATH, Xpath.State)
        txt_Pin = driver.find_element(By.XPATH, Xpath.Pin)
        txt_Mobile_number = driver.find_element(By.XPATH, Xpath.Mobile_Number)
        txt_Email = driver.find_element(By.XPATH, Xpath.email)
        txt_PassBank = driver.find_element(By.XPATH, Xpath.Pass_Bank)
        btnSubmit = driver.find_element(By.XPATH, Xpath.Submit_New_Bank)
        txt_Name.send_keys(Values.txt_Customer_Name)
        txt_Gender_female.click()
        txt_Data_of_Birth.send_keys(Values.txt_Date_of_Birth)
        txt_Address.send_keys(Values.txt_Address)
        txt_City.send_keys(Values.txt_City)
        txt_State.send_keys(Values.txt_State)
        txt_Pin.send_keys(Values.txt_Pin)
        txt_Mobile_number.send_keys(Values.txt_Mobile_Number)
        txt_Email.send_keys(Values.txt_email)
        txt_PassBank.send_keys(Values.txt_Pass_Bank)
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 3/Verify_add_new_customer.png'
        driver.save_screenshot(the_path)
        btnSubmit.click()
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 3/Verify_add_new_customer_submit.png'
        driver.save_screenshot(the_path)
        # Verify add new customer's information correct with value input above
        table_Name = driver.find_element(By.XPATH, Xpath.Table_Customer_Name).text
        table_Gender = driver.find_element(By.XPATH, Xpath.Table_Gender).text
        table_Address = driver.find_element(By.XPATH,Xpath.Table_Address).text
        table_City = driver.find_element(By.XPATH,Xpath.Table_City).text
        table_State = driver.find_element(By.XPATH,Xpath.Table_State).text
        table_Pin = driver.find_element(By.XPATH,Xpath.Table_Pin).text
        table_Mobile = driver.find_element(By.XPATH, Xpath.Table_Mobile).text
        table_Email = driver.find_element(By.XPATH,Xpath.Table_Email).text
        isExist = False
        try:
            self.assertIn(Values.txt_Customer_Name,table_Name)
            self.assertIn(Values.txt_Gender,table_Gender)
            self.assertIn(Values.txt_Address,table_Address)
            self.assertIn(Values.txt_City, table_City)
            self.assertIn(Values.txt_State, table_State)
            self.assertIn(Values.txt_Pin, table_Pin)
            self.assertIn(Values.txt_Mobile_Number, table_Mobile)
            self.assertIn(Values.txt_email, table_Email)
            isExist = True
            print("===========")
            print ("Verify add new customer's information correct with value input above: ", isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify add new customer's information correct with value input above: ", isExist)
            print("===========")
        # Logout and Verify login page displays
        btnLogout = driver.find_element(By.XPATH, Xpath.Log_out)
        driver.execute_script("arguments[0].click();", btnLogout)
        alert = Alert(driver)
        print (alert.text)
        alert.accept()
        isExist2 = False
        try:
            self.assertIn(Values.title_home_page_login, driver.title)
            isExist2 = True
            print("===========")
            print ("Verify login page displays: ", isExist2)
            print("===========")
        except:
            isExist2 = False
            print("===========")
            print ("Verify login page displays: ", isExist2)
            print("===========")
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()