import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import Xpath
import Values

class VerifyTheLoginSection(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(Xpath.url)
    def test_case(self):
        driver = self.driver
        userId = driver.find_element(By.XPATH, Xpath.userId)
        password = driver.find_element(By.XPATH, Xpath.password)
        btnLogin = driver.find_element(By.XPATH,Xpath.btnLogin)
        btnRest = driver.find_element(By.XPATH,Xpath.btnreset)
        # Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty
        userId.click()
        password.click()
        btnLogin.click()
        the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword/Exercise 1/VerifyTheMessageIsSeen.png'
        driver.save_screenshot(the_path)
        message_id_blank = driver.find_element(By.XPATH,Xpath.Blank_id)
        message_pass_blank = driver.find_element(By.XPATH,Xpath.Blank_pass)
        message_id = message_id_blank.is_displayed()
        message_pass = message_pass_blank.is_displayed()
        isExist = False
        if message_id == True and message_pass == True:
            isExist = True
            print("===========")
            print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty: ", isExist)
            print("===========")
        else:
            isExist = False
            print("===========")
            print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty: ", isExist)
            print("===========")
        # Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared
        userId.send_keys(Values.userId)
        password.send_keys(Values.password)
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 1/VerifyTheMessageHasDisappeared.png'
        driver.save_screenshot(the_path)
        message_id_blank = driver.find_element(By.XPATH,Xpath.Blank_id)
        message_pass_blank = driver.find_element(By.XPATH,Xpath.Blank_pass)
        message_id = message_id_blank.is_displayed()
        message_pass = message_pass_blank.is_displayed()
        isExist = False
        if message_id == False and message_pass == False:
            isExist = True
            print("===========")
            print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared: ", isExist)
            print("===========")
        else:
            isExist = False
            print("===========")
            print ("Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared: ", isExist)
            print("===========")
        # Verify userID and password is cleared
        btnRest.click()
        the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword/Exercise 1/VerifyClickButtonRest.png'
        driver.save_screenshot(the_path)
        blank_user = userId.text
        blank_pass = password.text
        isExist = False
        if not blank_user and not blank_pass:
            isExist = True
            print("===========")
            print ("Verify userID and password is cleared: ", isExist)
            print("===========")
        else:
            isExist = False
            print("===========")
            print ("Verify userID and password is cleared: ", isExist)
            print("===========")
        # Verify login to home page successful
        userId.send_keys(Values.userId)
        password.send_keys(Values.password)
        btnLogin.click()
        userID_login = driver.find_element(By.XPATH,Xpath.userId_login ).text
        the_path = 'D:/Intern TMA/Git/exercise-git-and-xpath/Exercise Python Keyword/Exercise 1/VerifytheLoginSection.png'
        driver.save_screenshot(the_path)
        isExist = False
        try:
            isExist = True
            self.assertIn("Guru99 Bank Manager HomePage",driver.title)
            print("===========")
            print("Verify dynamic userID in home page - UserID", userID_login)
            print ("Verify login to home page successful", isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify login to home page successful", isExist)
            print("===========")
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()