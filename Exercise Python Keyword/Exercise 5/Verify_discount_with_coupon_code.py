import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import Xpath
import Values

class VerifyDiscountWithCouponCode(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(Xpath.url)
    def test_case(self):
        driver = self.driver
        Mobile_Menu = driver.find_element(By.XPATH, Xpath.Mobile_menu)
        Mobile_Menu.click()
        Add_Iphone = driver.find_element(By.XPATH, Xpath.Btn_Add_to_iphone)
        Add_Iphone.click()
        btn_Continue = driver.find_element(By.XPATH, Xpath.btn_continue)
        btn_Continue.click()
        Add_Samsung = driver.find_element(By.XPATH, Xpath.Btn_Add_to_samsung)
        Add_Samsung.click()
        Update_number_samsung = driver.find_element(By.XPATH,Xpath.number_samsung_cart)
        Update_number_samsung.clear()
        Update_number_samsung.send_keys("2")
        btn_Update = driver.find_element(By.XPATH,Xpath.btn_update)
        btn_Update.click()
        code_Cart = driver.find_element(By.XPATH, Xpath.code_cart)
        code_Cart.send_keys(Values.txt_code)
        btn_Apply = driver.find_element(By.XPATH,Xpath.btn_apply_code)
        btn_Apply.click()
        # Verify discount value base on coupon code
        Subtotal = driver.find_element(By.XPATH,Xpath.Subtotal).text
        Discount = driver.find_element(By.XPATH,Xpath.Discount).text
        number_Subtotal = int(Subtotal[1:-3])
        number_Discount = int(Discount[2:-3])
        discount_value =  number_Discount/number_Subtotal  * 100
        print ("Discount value base on coupon code:",discount_value ,"%")
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 5/Verify_discount_with_coupon_code.png'
        driver.save_screenshot(the_path)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()