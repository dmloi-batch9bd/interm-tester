from select import select
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import Xpath

class VerifyTheMessageIsSeen(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(Xpath.url)
    def test_case(self):
        driver = self.driver
        Mobile_menu = driver.find_element(By.XPATH,Xpath.Mobile_menu)
        Mobile_menu.click()
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 4/VerifyXpathAxesMethod.png'
        driver.save_screenshot(the_path)
        select = driver.find_element(By.XPATH,Xpath.select)
        select_option = Select(select)
        get_select = select_option.first_selected_option
        #Verify sort by position selected
        isExist = False
        try:
            self.assertIn("Position",get_select.text)
            isExist = True
            print("===========")
            print ("Verify sort by position: ",get_select.text,"-", isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify sort by position: ", isExist)
            print("===========")
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 4/Check.png'
        driver.save_screenshot(the_path)
        # Locate the name
        ul_1st_place = driver.find_element(By.XPATH,Xpath.ul_1st_place)
        ul_2st_place = driver.find_element(By.XPATH,Xpath.ul_2st_place)
        ul_3st_place = driver.find_element(By.XPATH,Xpath.ul_3st_place)
        # Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 4/CheckPosition.png'
        driver.save_screenshot(the_path)
        isExist = False
        try:
            self.assertIn("SONY XPERIA",ul_1st_place.text)
            self.assertIn("IPHONE",ul_2st_place.text)
            self.assertIn("SAMSUNG GALAXY",ul_3st_place.text)
            isExist = True
            print("===========")
            print ("Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY: ", isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY: ", isExist)
            print("===========")
        # Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA
        select_Name = Select(driver.find_element(By.XPATH,Xpath.select))
        select_Name.select_by_visible_text("Name")
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 4/CheckName.png'
        driver.save_screenshot(the_path)
        ul_1st_place = driver.find_element(By.XPATH,Xpath.ul_1st_place)
        ul_2st_place = driver.find_element(By.XPATH,Xpath.ul_2st_place)
        ul_3st_place = driver.find_element(By.XPATH,Xpath.ul_3st_place)
        isExist = False
        try:
            self.assertIn("IPHONE",ul_1st_place.text)
            self.assertIn("SAMSUNG GALAXY",ul_2st_place.text)
            self.assertIn("SONY XPERIA",ul_3st_place.text)
            isExist = True
            print("===========")
            print ("Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA: ", isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA: ", isExist)
            print("===========")
        # Verify sort by price 1st Sony Explorer 2st SAMSUNG GALAXY 3st IPHONE
        select_Price = Select(driver.find_element(By.XPATH,Xpath.select))
        select_Price.select_by_visible_text("Price")
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 4/Checkprice.png'
        driver.save_screenshot(the_path)
        ul_1st_place = driver.find_element(By.XPATH,Xpath.ul_1st_place)
        ul_2st_place = driver.find_element(By.XPATH,Xpath.ul_2st_place)
        ul_3st_place = driver.find_element(By.XPATH,Xpath.ul_3st_place)
        isExist = False
        try:
            self.assertIn("SONY XPERIA",ul_1st_place.text)
            self.assertIn("SAMSUNG GALAXY",ul_2st_place.text)
            self.assertIn("IPHONE",ul_3st_place.text)
            isExist = True
            print("===========")
            print ("Verify sort by price 1st SONY XPERIA 2st SAMSUNG GALAXY 3st IPHONE: ", isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify sort by price 1st SONY XPERIA 2st SAMSUNG GALAXY 3st IPHONE: ", isExist)
            print("===========")
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()