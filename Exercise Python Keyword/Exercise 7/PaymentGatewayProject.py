import unittest
import re
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
import Xpath

class VerifyPaymentGatewayProject(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(Xpath.url)
    def test_case(self):
        driver = self.driver
        Payment_Gateway_Project = driver.find_element(By.XPATH,Xpath.Payment_Gateway_Project)
        Payment_Gateway_Project.click()
        element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH,Xpath.Generate_Card_Number)))
        Generate_Card_Number = driver.find_element(By.XPATH, Xpath.Generate_Card_Number)
        Generate_Card_Number.click()
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        #Get Text Your Card Number
        Card_number = driver.find_element(By.XPATH, Xpath.Card_number).text
        Card_CW = driver.find_element(By.XPATH, Xpath.Card_CW).text
        Card_Exp = driver.find_element(By.XPATH, Xpath.Card_Exp).text
        number_card = re.sub(r'\D', "", Card_number)
        cw_card = re.sub(r'\D', "", Card_CW)
        Exp = re.sub(r'\D', "", Card_Exp)
        exp_year = Exp[2:]
        exp_month = Exp[:-4]
        # Check your Card
        Check_Credit_Card = driver.find_element(By.XPATH,Xpath.Check_Credit_Card)
        Check_Credit_Card.click()
        element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH,Xpath.Enter_Credit_Card)))
        Enter_Credit_Card = driver.find_element(By.XPATH,Xpath.Enter_Credit_Card)
        Enter_Credit_Card.send_keys(number_card)
        btn__Credit_Card = driver.find_element(By.XPATH,Xpath.btn__Credit_Card)
        btn__Credit_Card.click()
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 7/Check your Card.png'
        driver.save_screenshot(the_path)
        driver.close()
        window_after = driver.window_handles[0]
        driver.switch_to.window(window_after)
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 7/Back.png'
        driver.save_screenshot(the_path)
        # Select Quantity and click on BUY NOW and Input info and submit
        select = Select(driver.find_element(By.XPATH,Xpath.Quantity))
        select.select_by_visible_text("2")
        btn_Buy_Now = driver.find_element(By.XPATH,Xpath.btn_Buy_Now)
        btn_Buy_Now.click()
        buy_card_number = driver.find_element(By.XPATH,Xpath.buy_card_number)
        buy_card_number.send_keys(number_card)
        select_exp_month = Select(driver.find_element(By.XPATH,Xpath.Expiration_Month))
        select_exp_month.select_by_visible_text(exp_month)
        select_exp_year = Select(driver.find_element(By.XPATH,Xpath.Expiration_Year))
        select_exp_year.select_by_visible_text(exp_year)
        CW_code = driver.find_element(By.XPATH,Xpath.CW_code)
        CW_code.send_keys(cw_card)
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 7/SelectQuantity.png'
        driver.save_screenshot(the_path)
        btn_pay = driver.find_element(By.XPATH,Xpath.btn_pay)
        btn_pay.click()
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 7/Exercise 7.png'
        driver.save_screenshot(the_path)
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()