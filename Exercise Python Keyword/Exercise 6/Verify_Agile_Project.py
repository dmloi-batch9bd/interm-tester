import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
import Xpath
import Values

class VVerifyAgileProject(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(Xpath.url)
    def test_case(self):
        driver = self.driver
        Agile_Project = driver.find_element(By.XPATH, Xpath.Agile_Project)
        Agile_Project.click()
        userId = driver.find_element(By.XPATH, Xpath.userId)
        password = driver.find_element(By.XPATH, Xpath.password)
        btnLogin = driver.find_element(By.XPATH, Xpath.btnLogin)
        userId.send_keys(Values.txt_user_Agile)
        password.send_keys(Values.txt_pass_Agile)
        btnLogin.click()
        Mini_Statement = driver.find_element(By.XPATH, Xpath.Mini_Statement)
        Mini_Statement.click()
        select = Select(driver.find_element(By.XPATH,Xpath.Mini_Account))
        select.select_by_visible_text(Values.Account_no)
        submit = driver.find_element(By.XPATH, Xpath.Submit_Agile)
        submit.click()
        the_path = 'D:/Intern TMA/Exercise/Exercise Python Keyword/Exercise 6/VVerifyAgileProjectv.png'
        driver.save_screenshot(the_path)
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()