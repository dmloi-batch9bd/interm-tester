import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import Xpath

class VerifyRowTableXpathAxesMethod(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(Xpath.url)
    def test_case(self):
        driver = self.driver
        Action = driver.find_element(By.XPATH, Xpath.Action)
        Bank_Name = driver.find_element(By.XPATH,Xpath.Bank_Name)
        # Verify position of Action
        # Verify column Action stay on the left of column Bank Name
        isExist = False
        try:
            self.assertIn("Action", Action.text)
            location = Action.location
            isExist = True
            print("===========")
            print ("Verify position of Action: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify position of Action: ", isExist)
            print("===========")
        # Verify position of Bank Name
        # Verify column Bank Name stay on the right of column Action
        try:
            self.assertIn("Bank Name", Bank_Name.text)
            location = Bank_Name.location
            isExist = True
            print("===========")
            print ("Verify position of Bank Name: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify position of Bank Name: ", isExist)
            print("===========")
        # Verify cell Bank 3 stay under column Bank Name
        Bank_3 = driver.find_element(By.XPATH, Xpath.Bank3)
        try:
            self.assertIn("Bank 3", Bank_3.text)
            location = Bank_3.location
            isExist = True
            print("===========")
            print ("Verify cell Bank 3 stay under column Bank Name: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify cell Bank 3 stay under column Bank Name: ", isExist)
            print("===========")
        # Verify cell Edit stay on the left of cell bank 3
        Edit_Bank_3 = driver.find_element(By.XPATH,Xpath.Edit_Bank3)
        try:
            self.assertIn("Edit", Edit_Bank_3.text)
            location = Edit_Bank_3.location
            isExist = True
            print("===========")
            print ("Verify cell Edit stay on the left of cell bank 3: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Verify cell Edit stay on the left of cell bank 3: ", isExist)
            print("===========")
        # Row 1 should be Bank 1 under Bank Name column
        Row_1 = driver.find_element(By.XPATH,Xpath.row_1)
        try:
            self.assertIn("Edit Bank 1", Row_1.text)
            location = Row_1.location
            isExist = True
            print("===========")
            print ("Row 1 should be Bank 1 under Bank Name column: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Row 1 should be Bank 1 under Bank Name column: ", isExist)
            print("===========")
        # Row 2 should be Bank 1 under Bank Name column
        Row_2 = driver.find_element(By.XPATH,Xpath.row_2)
        try:
            self.assertIn("Edit Bank 2", Row_2.text)
            location = Row_1.location
            isExist = True
            print("===========")
            print ("Row 2 should be Bank 2 under Bank Name column: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Row 2 should be Bank 2 under Bank Name column: ", isExist)
            print("===========")
        # Row 3 should be Bank 3 under Bank Name column
        Row_3 = driver.find_element(By.XPATH,Xpath.row_3)
        try:
            self.assertIn("Edit Bank 3", Row_3.text)
            location = Row_3.location
            isExist = True
            print("===========")
            print ("Row 3 should be Bank 3 under Bank Name column: ",location, isExist)
            print("===========")
        except:
            isExist = False
            print("===========")
            print ("Row 3 should be Bank 3 under Bank Name column: ", isExist)
            print("===========")
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()