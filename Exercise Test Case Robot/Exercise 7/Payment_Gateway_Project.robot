*** Settings ***
Library    Selenium2Library
Library    String
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify page "Payment Gateway Project"
Test Timeout    2 minutes
*** Test Cases ***
Exercise 7
    [Documentation]
    ...    - Go to URL: http://demo.guru99.com/
    ...    - Click on Payment Gateway Project
    ...    - Click on "Generate Card Number" and get your card number ID
    ...    - Click on "Check Credit Card Linit" to check your Card
    ...    - Back to Payment Gateway Project
    ...    - Select Quantity and click on BUY NOW
    ...    - Input info and submit
    Open Browser    ${Browser_Exe7}    ${device}
    Maximize Browser Window
    Click Link    ${Payment_Gateway_Project}
    Click Link    ${Generate_Card_Number}
    Switch Window    new
    Get Text Your Card Number
    Wait Until Element Is Visible    ${Check_Credit_Card}
    Click Link    ${Check_Credit_Card}
    Wait Until Element Is Visible    ${Enter_Credit_Card}
    Input Text    ${Enter_Credit_Card}    ${number_card}
    Click Button    ${btn__Credit_Card}
    Close Window
    Switch Window
    Select From List By Label    ${Quantity}    2
    Click Button    ${btn_Buy_Now}
    Input Text    ${buy_card_number}    ${number_card}
    Select From List By Label    ${Expiration_Month}    ${exp_month}
    Select From List By Label    ${Expiration_Year}    ${exp_year}
    Input Text    ${CW_code}    ${cw_card}
    Capture Page Screenshot   input.png
    Click Button    ${btn_pay}
*** Keywords ***
Get Text Your Card Number
    ${txt_card_number}    Get Text    ${Card_number}
    ${txt_card_cw}    Get Text    ${Card_CW}
    ${txt_card_exp}    Get Text    ${Card_Exp}
    ${exp_my}    Fetch From Right    ${txt_card_exp}    -${SPACE}
    ${number_card}    Fetch From Right    ${txt_card_number}    -${SPACE}
    Set Suite Variable    ${number_card}
    ${cw_card}    Fetch From Right    ${txt_card_cw}    -${SPACE}
    Set Suite Variable    ${cw_card}
    ${exp_year}    Fetch From Right    ${exp_my}    /
    Set Suite Variable    ${exp_year}
    ${exp_month}    Fetch From Left    ${exp_my}    /
    Set Suite Variable    ${exp_month}
    






    
