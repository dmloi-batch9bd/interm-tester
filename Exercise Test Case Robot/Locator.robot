*** Settings ***
Library    Selenium2Library
*** Variables ***
#exe1
${Browser}    https://demo.guru99.com/v4/
${Device}    Chrome
${userId}    //*[@name="uid"]
${password}    //*[@name="password"]
${btnLogin}    //*[@name="btnLogin"]
${btnreset}    //*[@name="btnReset"]
${Blank_id}    //*[@id="message23"]
${Blank_pass}    //*[@id="message18"]
${userId_login}    //*[@class="heading3"]//child::td
${url_login_home_page}    https://demo.guru99.com/v4/manager/Managerhomepage.php
#exe2
${Browser_exe2}    file:///D:/Intern%20TMA/Exercise/Exercise%20Test%20Case%20Robot/Exercise%202/axes.html
${Device}    Chrome
${Action}    //table[@id="BankList"]//child::th[1]
${Bank_Name}    //table[@id="BankList"]//child::th[2]
${Bank3}    //*[text()='Bank Name']//following::td[6]
${Edit_Bank3}    //*[text()="Bank 3"]//preceding::td[1]
${row_1}    //*[text()="Bank 1"]//ancestor::tr
${row_2}    //*[text()="Bank 2"]//ancestor::tr
${row_3}    //*[text()="Bank 3"]//ancestor::tr
${row_4}    //*[text()="Bank 4"]//ancestor::tr
#exe3
${New_Customer}    //a[@href="addcustomerpage.php"]
${Customer_Name}    //*[@name="name"]
${Gender_female}    //*[@name="rad1" and @value="f"]
${Gender_male}    //*[@name="rad1" and @value="m"]
${Date_of_Birth}    //*[@name="dob"]
${Address}    //*[@name="addr"]
${City}    //*[@name="city"]
${State}    //*[@name="state"]
${Pin}    //*[@name="pinno"]
${Mobile_Number}    //*[@name="telephoneno"]
${email}    //*[@name="emailid"]
${Pass_Bank}    //*[@name="password"]
${Submit_New_Bank}    //*[@name="sub" and @value="Submit"]
${Table_Customer_Name}    //td[text()="Customer Name"]//following::td[1]
${Table_Gender}    //td[text()="Gender"]//following::td[1]
${Table_Address}    //td[text()="Address"]//following::td[1]
${Table_City}    //td[text()="City"]//following::td[1]
${Table_State}    //td[text()="State"]//following::td[1]
${Table_Pin}    //td[text()="Pin"]//following::td[1]
${Table_Mobile}    //td[text()="Mobile No."]//following::td[1]
${Table_Email}    //td[text()="Email"]//following::td[1]
${Log_out}    //a[text()="Log out"]
#exe4
${Browser_Exe4}    http://live.guru99.com
${select}    //select[@title="Sort By"]
${ul_1st_place}    //ul[@class="products-grid products-grid--max-4-col first last odd"]//child::li[@class="item last"][1]
${ul_2st_place}    //ul[@class="products-grid products-grid--max-4-col first last odd"]//child::li[@class="item last"][2]
${ul_3st_place}    //ul[@class="products-grid products-grid--max-4-col first last odd"]//child::li[@class="item last"][3]
#exe5
${Browser_Exe5}    https://live.guru99.com
${Mobile_menu}    //a[text()="Mobile"]
${Btn_Add_to_iphone}    //*[text()="IPhone"]//following::button[1]
${btn-continue}    //*[@class="button2 btn-continue"]
${Btn_Add_to_samsung}    //*[text()="Samsung Galaxy"]//following::button[1]
${number_samsung_cart}    //a[text()="Samsung Galaxy"]//following::td[15]//child::input
${btn_update}    //a[text()="Samsung Galaxy"]//following::td[15]//child::button
${code_cart}    //input[@name="coupon_code"]
${btn_apply_code}    //button[@class="button2" and @value="Apply"]
${Subtotal}    //td[contains(text(),'Subtotal')]//following::span[1]
${Discount}    //td[contains(text(),'Discount')]//following::span[1]
#exe6
${Browser_Exe6}    https://demo.guru99.com/
${Agile_Project}    //a[text()="Agile Project"]
${Mini_Statement}    //a[text()="Mini Statement"]
${Mini-Account}    //select[@name="accountno"]
#exe7
${Browser_Exe7}    https://demo.guru99.com/#google_vignette
${Payment_Gateway_Project}    //a[text()="Payment Gateway Project"]
${Generate_Card_Number}    //*[@id="nav"]//child::a[2]
${Card_number}    //*[@class="align-center"]//following::h4[1]
${Card_CW}    //*[@class="align-center"]//following::h4[2]
${Card_Exp}    //*[@class="align-center"]//following::h4[3]
${Check_Credit_Card}    //*[@id="nav"]//child::a[3]
${Enter_Credit_Card}    //input[@id="card_nmuber"]
${btn__Credit_Card}    //input[@type="submit"]
${Quantity}    //*[@name="quantity"]
${btn_Buy_Now}    //input[@value="Buy Now"]
${buy_card_number}    //input[@name="card_nmuber"]
${Expiration_Month}    //select[@name="month"]
${Expiration_Year}    //select[@name="year"]
${CW_code}    //input[@name="cvv_code"]
${btn_pay}    //input[@name="submit"]

