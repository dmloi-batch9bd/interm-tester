*** Settings ***
Library    Selenium2Library
Library    Telnet
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify the order of column and row in table  using xpath axes method
Test Timeout    2 minutes
*** Test Cases ***
Exercise 2
    [Documentation]
    ...    - Verify position of Action
    ...    - Verify position of Bank Name
    ...    - Verify column Action stay on the left of column Bank Name
    ...    - Verify column Bank Name stay on the right of column Action
    ...    - Verify cell Bank 3 stay under column Bank Name
    ...    - Verify cell Edit stay on the left of cell bank 3
    ...    - Verify row 1 should be Bank 1 under Bank Name column
    ...    - Verify row 2 should be Bank 3 under Bank Name column
    ...    - Verify row 3 should be Bank 3 under Bank Name column
    ...    - Verify row 4 should be Bank 4 under Bank Name column
    Open Browser    ${Browser_exe2}    ${Device}
    Verify position of Action
    Verify position of Bank Name
    Verify column Action stay on the left of column Bank Name
    Verify column Bank Name stay on the right of column Action
    Verify cell Bank 3 stay under column Bank Name
    Verify cell Edit stay on the left of cell bank 3
    Verify row 1 should be Bank 1 under Bank Name column
    Verify row 2 should be Bank 2 under Bank Name column
    Verify row 3 should be Bank 3 under Bank Name column
    Verify row 4 should be Bank 4 under Bank Name column
*** Keywords ***
Verify position of Action
    Element Should Contain    ${Action}    Action
    ${H_position_ACtion}    Get Horizontal Position    ${Action}
    ${V_positon_Action}    Get Vertical Position    ${Action}
Verify position of Bank Name
    Element Should Contain    ${Bank_Name}    Bank Name
    ${H_position_ACtion}    Get Horizontal Position    ${Bank_Name}
    ${V_positon_Action}    Get Vertical Position    ${Bank_Name}
Verify column Action stay on the left of column Bank Name
    Page Should Contain Element    ${Action}
Verify column Bank Name stay on the right of column Action
    Page Should Contain Element    ${Bank_Name}
Verify cell Bank 3 stay under column Bank Name
    Page Should Contain Element    ${Bank3}
    Element Should Contain    ${Bank3}    Bank 3
Verify cell Edit stay on the left of cell bank 3
    Page Should Contain Element    ${Edit_Bank3}
    Element Should Contain    ${Edit_Bank3}    Edit
Verify row 1 should be Bank 1 under Bank Name column
    Page Should Contain Element    ${row_1}
Verify row 2 should be Bank 2 under Bank Name column
    Page Should Contain Element    ${row_2}
Verify row 3 should be Bank 3 under Bank Name column
    Page Should Contain Element    ${row_3}
Verify row 4 should be Bank 4 under Bank Name column
    Page Should Contain Element    ${row_4}