*** Settings ***
Library    Selenium2Library
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify Agile project
Test Timeout    2 minutes
*** Test Cases ***
Exercise 6
    [Documentation]
    ...    - Go to URL: http://demo.guru99.com/
    ...    - Click to Agile Project
    ...    - Login to Agile Project with user/pass from this page.
    ...    - Click Mini Statement
    ...    - Select Account and click submit
    ...    - Verify detail info of a Transaction ID. (F)
    Open Browser    ${Browser_Exe6}#google_vignette    ${Device}
    Click Link    ${Agile_Project}
    Input Text    ${userId}    ${txt_user_Agile}
    Input Password    ${password}    ${txt_pass_Agile}
    Click Button    ${btnLogin}
    Click Link    ${Mini_Statement}
    Select From List By Label    ${Mini-Account}    ${Account_no}