*** Settings ***
Library    Selenium2Library
Library    String
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify sort order using xpath Axes method
Test Timeout    2 minutes
*** Test Cases ***
Exercise 4
    [Documentation]    Verify sort order using xpath Axes method
    ...    - Go to URL: http://live.guru99.com
    ...    - Click on Mobile menu
    ...    - Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY
    ...    - Click sort by Name
    ...    - Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA
    ...    - Click sort by Price
    ...    - Verify sort by price 1st Sony Explorer 2st SAMSUNG GALAXY 3st IPHONE
    Open Browser    ${Browser_Exe4}    ${Device}
    Click Element    ${Mobile_menu}
    List Selection Should Be    ${select}    Position
    Run Keyword And Continue On Failure     Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY   
    Select From List By Label    ${select}    Name
    Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA
    Select From List By Label    ${select}    Price
    Verify sort by price 1st Sony Explorer 2st SAMSUNG GALAXY 3st IPHONE
*** Keywords ***
Verify sort by position 1st Sony Explorer 2st IPHONE 3st SAMSUNG GALAXY
    Element Should Contain    ${ul_1st_place}    SONY XPERIA
    Element Should Contain    ${ul_2st_place}    IPHONE
    Element Should Contain    ${ul_3st_place}    SAMSUNG GALAXY
Verify sort by name 1st IPHONE 2st SAMSUNG GALAXY 3st SONY XPERIA
    Element Should Contain    ${ul_1st_place}    IPHONE
    Element Should Contain    ${ul_2st_place}    SAMSUNG GALAXY
    Element Should Contain    ${ul_3st_place}    SONY XPERIA
Verify sort by price 1st Sony Explorer 2st SAMSUNG GALAXY 3st IPHONE
    Element Should Contain    ${ul_1st_place}    SONY XPERIA
    Element Should Contain    ${ul_2st_place}    SAMSUNG GALAXY
    Element Should Contain    ${ul_3st_place}    IPHONE
