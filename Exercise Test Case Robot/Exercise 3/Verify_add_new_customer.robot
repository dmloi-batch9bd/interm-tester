*** Settings ***
Library    Selenium2Library
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify add new customer
Test Timeout    2 minutes
*** Test Cases ***
Exercise 3
    [Documentation]
    ...    - Login home page
    ...    - Select new customer
    ...    - Fill all fields
    ...    - Submit form
    ...    - Verify add new customer's information correct with value input above
    ...    - Logout
    ...    - Verify login page displays
    ...    - Evidence screenshots are required for verification steps
    Open Browser    ${Browser}    ${Device}
    Input Text    ${userId}    ${txt_userId_true}
    Input Password    ${password}    ${txt_pass_true}
    Click Button    ${btnLogin}
    Click Link    ${New_Customer}
    Input Text    ${Customer_Name}    ${txt_Customer_Name}
    Click Element    ${Gender_female}
    Input Text    ${Date_of_Birth}    ${txt_Date_of_Birth}
    Input Text    ${Address}    ${txt_Address}
    Input Text    ${City}    ${txt_City}
    Input Text    ${State}    ${txt_State}
    Input Text    ${Pin}    ${txt_Pin}
    Input Text    ${Mobile_Number}    ${txt_Mobile_Number}
    Input Text    ${email}    ${txt_email}
    Input Text    ${Pass_Bank}    ${txt_Pass_Bank}
    Click Button    ${Submit_New_Bank}
    Element Text Should Be    ${Table_Customer_Name}    ${txt_Customer_Name}
    Element Text Should Be    ${Table_Gender}    ${txt_Gender}
    Element Text Should Be    ${Table_Address}    ${txt_Address}
    Element Text Should Be    ${Table_City}    ${txt_City}
    Element Text Should Be    ${Table_State}    ${txt_State}
    Element Text Should Be    ${Table_Pin}    ${txt_Pin}
    Element Text Should Be    ${Table_Mobile}    ${txt_Mobile_Number}
    Element Text Should Be    ${Table_Email}    ${txt_email}
    Capture Page Screenshot   Verify_add_new_customer.png
    Click Element At Coordinates    ${Log_out}    50    16
    Handle Alert
    Title Should Be    ${title_home_page_login}
    Capture Page Screenshot    logout.png