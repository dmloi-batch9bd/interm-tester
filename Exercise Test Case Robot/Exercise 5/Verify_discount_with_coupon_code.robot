*** Settings ***
Library    Selenium2Library
Library    String
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify discount with coupon code
Test Timeout    2 minutes
*** Test Cases ***
Exercise 5
    [Documentation]
    ...    - Go to URL: http://live.guru99.com
    ...    - Click on Mobile menu
    ...    - Add IPHONE to cart
    ...    - Add SAMSUNG GALAXY to cart
    ...    - Update quantity to 2 for SAMSUNG GALAXY
    ...    - Input coupon GURU50 (5%)
    ...    - Verify discount value base on coupon code
    Open Browser    ${Browser_Exe5}    ${Device}
    Click Link    ${Mobile_menu}
    Click Element    ${Btn_Add_to_iphone}
    Click Button    ${btn-continue}
    Click Button    ${Btn_Add_to_samsung}
    Click Element    ${number_samsung_cart}
    Input Text    ${number_samsung_cart}    2
    Click Button    ${btn_update}
    Input Text    ${code_cart}    ${txt_code}
    Click Element    ${btn_apply_code}
    Verify discount value base on coupon code
*** Keywords ***
Verify discount value base on coupon code
    ${number_Subtotal}    Get Text    ${Subtotal}
    ${number_Discount}    Get Text    ${Discount}
    ${txt_Subtotal}    Fetch From Right    ${number_Subtotal}   $
    ${txt_Discount}    Fetch From Right    ${number_Discount}    $
    ${discount_value}=   Evaluate    ${txt_Discount}/${txt_Subtotal}*100
    
