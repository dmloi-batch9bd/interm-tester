*** Settings ***
Library    Selenium2Library
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Variables.robot
Resource    D:/Intern TMA/Exercise/Exercise Test Case Robot/Locator.robot
Documentation    Verify the Login Section
Test Timeout    2 minutes
*** Test Cases ***
Test Case 1
    [Documentation]
    ...    - Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty
    ...    - Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared
    ...    - Verify userID and password is cleared    
    [Tags]    TC01
    Open Browser    ${Browser}    ${Device}
    Click Element    ${userId}
    Click Element    ${password}
    Click Button    ${btnLogin}
    Element Should Be Visible    ${Blank_id}
    Element Should Be Visible    ${Blank_pass}
    Input Text    ${userId}    ${txt_userId_false}
    Input Text    ${password}    ${txt_pass_false}
    Element Should Not Be Visible    ${Blank_id}
    Element Should Not Be Visible    ${Blank_pass}
    Click Button    ${btnreset}
    Current Frame Should Not Contain    ${userId}
    Current Frame Should Not Contain    ${password}
Test Case 2
    [Documentation]
    ...    - Verify login to home page successful    
    ...    - Verify dynamic userID in home page    
    ...    - Evidence screenshots are required for verification steps
    [Tags]    TC02
    Open Browser    ${Browser}    ${Device}
    Input Text    ${userId}    ${txt_userId_true}
    Input Text    ${password}    ${txt_pass_true}
    Click Button    ${btnLogin}
    Title Should Be    ${title_home_page}
    Element Should Contain    ${userId_login}    ${txt_userId_true}
    ${txt_id}    Get Text    ${userId_login}
    Capture Page Screenshot    Login.png